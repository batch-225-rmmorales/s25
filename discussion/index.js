//json objects
/*
json stands for javascript object notation
also used in other programming languages
hence the name javascript

syntax:
{
	"propertyA":valueA,
	"propertyB":valueB,
	...
	"propertyZ":valueZ
}
*/
// JSON
let mors = {
	"city": "quezon City",
	"province": "metro manila"
}
// [SECTION] JSON Arrays
/*
"cities": [
    { "city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Manila City", "province": "Metro Manila", "country": "Philippines" },
    { "city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
]
*/

// json methods
// json object contains methods for parsing and converting data into stringified json

// converting data into stringified json

/*
stringified json is a javascript object converted intro string to be used in other functions of a javascript application
they are commonly used in http request wwhere information is required to be send and rcvd in a stringified json format
request are an important part of programming where application communicates with a backend application to perform different tasks such as getting/createing data in a dattabase
*/

let batchesArr = [{batchName: 'batch x'}, {batchName: 'batch y'}];

console.log('result from stringify');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'john',
	age: 31,
	address: {
		city: "manila",
		country: "philippines"
	},
	fullname: function (){
		return this.name + this.age;
	}
});
console.log(data);
console.log(typeof data);

//user details

// let firstName = prompt('what is your first name');
// let lastName = prompt('what is your last name');
// let age = prompt('how old ar eyou?');
// let address = {
// 	city: prompt('which city do you live in'),
// 	country: prompt('which country does your city address belong to?')
// };

// let otherData = JSON.stringify({
// 	firstName:firstName,
// 	lastName:lastName,
// 	age:age,
// 	address:address
// });
// console.log(otherData);

// [Section] Converting stringified JSON into JavaScript objects
/*
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- Information is commonly sent to applications in stringified JSON and then converted back into objects
	- This happens both for sending information to a backend application and sending information back to a frontend application
	-Parsing means analyzing and converting a program into an internal format that a runtime environment can actually run
*/

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`

console.log(JSON.parse(stringifiedObject));

dataobject = JSON.parse(data);
console.log(dataobject);
console.log(typeof dataobject);